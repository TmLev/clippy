# Clippy

_CLI for C++(PP) courses written in PYthon_

[The Tragic Life of Clippy, the World's Most Hated Virtual Assistant](https://www.mentalfloss.com/article/504767/tragic-life-clippy-worlds-most-hated-virtual-assistant)

## Disclaimer

Цель клиента – упростить и унифицировать рутинные действия.

Клиент не стремится полностью скрыть от пользователя `git`, `cmake`, линтеры и другие инструменты разработки. Ожидается, что пользователь в минимальной степени владеет этими инструментами.

## Документация

- [Команды](docs/commands.md)
- [Схема работы](docs/diagram.png)
- [Конфигурация задачи](docs/task.md)
- [Конфигурация клиента](docs/config.md)

## Благодарности

Оригинальная идея клиента и первая реализация – [Степан Калинин](https://github.com/MrKaStep)


